FROM php:7.4.21-apache-buster

# App install
RUN apt-get update && apt-get install -y zlib1g-dev \
  libzip-dev \
  unzip \
  zip \
  libjpeg62-turbo-dev \
  libpng-dev \
  wget \
  libaio1 \
  libfreetype6-dev \
  rsync \
  zlib1g-dev \
  libxml2-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install required extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install ctype
RUN docker-php-ext-install fileinfo
RUN docker-php-ext-install json
RUN docker-php-ext-install tokenizer
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd
RUN docker-php-ext-install zip
RUN docker-php-ext-install opcache
RUN docker-php-ext-install soap
RUN docker-php-ext-install intl
RUN docker-php-ext-install xmlrpc

# Apache mod
RUN a2enmod rewrite
RUN a2enmod expires
RUN a2enmod ext_filter
RUN a2enmod headers